
(function(){
    angular
        .module("app.users")
       // .controller("DefaultAppCtrl", DefaultAppCtrl)
       // .controller("CustomAppCtrl", CustomAppCtrl)
        .controller("SignUpCtrl", SignUpCtrl)
        .controller("LoginCtrl", LoginCtrl)
        .controller("ModalCtrl", ModalCtrl, ["$scope"])
        .service("dbService", function () {

        })
       // .controller("ForgotPasswordCtrl", ForgotPasswordCtrl)
       // .controller("LogoutCtrl", LogoutCtrl)
        //.controller("ProfileCtrl", ProfileCtrl);

    function ModalCtrl($scope) {
        var vm = this;
        this.tab = {
            open: "login"
        }

        vm.isSignupEnabled = function() {
            console.log(vm.openTab)
            return vm.tab.open == "sign up"
        };

        vm.isLoginEnabled = function() {
            return vm.tab.open == "login"
        };

        vm.selectTab = function(tab) {
            vm.tab.open = tab
        }
    }

    function SignUpCtrl($http, $q, $rootScope, $sanitize, $state, dbService, AuthService, flash){
        var vm = this;
        vm.firstname = "";
        vm.lastname = "";
        vm.email = "";
        vm.password = "";
        
        vm.signup = function () {
            AuthService.signup($sanitize(vm.firstname), $sanitize(vm.lastname), $sanitize(vm.email), $sanitize(vm.password))
                .then(function () {
                    vm.disabled = false;
                    vm.firstname = "";
                    vm.lastname = "";
                    vm.email = "";
                    vm.password = "";
                    flash.clear();
                    flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    angular.element("#logInModal").modal('hide');
                    //$state.go("SignIn");
                }).catch(function () {
                    console.error("Sign Up is having issues");
            });
        };

        vm.isSignupEnabled = function() {
            console.log("firing")
            return $rootScope.openTab == "sign up"
        }

    }

    SignUpCtrl.$inject  = ["$http", "$q", "$rootScope", "$sanitize", "$state", "dbService", "AuthService", "flash"];

    function LoginCtrl($http, $q, $sanitize, $state, dbService, AuthService, flash){
        var vm = this;

        vm.login = function () {
            AuthService.login(vm.user)
                .then(function () {
                    if(AuthService.isLoggedIn()){
                        vm.email = "";
                        vm.password = "";
                        angular.element("#logInModal").modal('hide');
                        // $state.go("defaultApp");
                    }else{
                        flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        //$state.go("SignIn");
                    }
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }

    LoginCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService","flash"];

    function LogoutCtrl($http, $q, $sanitize, $state, dbService, AuthService){
        var vm = this;

        vm.logout = function () {
            AuthService.logout()
                .then(function () {
                    $state.go("SignIn");
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }

    LogoutCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService"];

    function ProfileCtrl($http, $q, $sanitize, $state, dbService){
        var vm = this;
        vm.sociallogins = [];
        dbService.getLocalProfile().then(function(result){
            vm.localProfile = result.data;
        });

        dbService.getAllSocialLoginsProfile().then(function(result){
            vm.sociallogins = result.data;
        });

    }

    ProfileCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService"];

    function ResetPasswordCtrl($http, $q, $sanitize,dbService){
        var vm = this;
        vm.emailAddress = "";

        vm.resetPassword = function () {
            console.info($sanitize(vm.emailAddress));

        };
    }

    ResetPasswordCtrl.$inject  = ["$http", "$q", "$sanitize", "dbService"];

    function DefaultAppCtrl($q, dbService, AuthService){
        var vm = this;
        vm.results = [];
        vm.pageIndex = 0;
        const recordPerPage = 3;
        vm.totalRecords = 0;
        vm.totalPages = 0;
        vm.currentPage = 1;

        AuthService.getUserStatus(function(result){
            // hack for social logins ...
            // don't really understand why angular js didn't move to the next line of codes.
            vm.isUserLogon = result;
        });
        vm.isUserLogon = AuthService.isLoggedIn();

        var defer = $q.defer();
        vm.err = null;
        if(vm.isUserLogon){
            dbService.getTotalDefaultApps();

            dbService.defaultAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });

            dbService.getTotalDefaultApps()
                .then(function (results) {
                    vm.totalRecords = results.data;
                    vm.totalPages = Math.ceil(vm.totalRecords/recordPerPage);
                })
                .catch(function (err) {
                    console.error(err);
                });


            vm.next = function () {
                vm.currentPage = vm.currentPage + 1;
                vm.pageIndex = vm.pageIndex + recordPerPage;
                dbService.defaultAppPaging(vm.pageIndex, recordPerPage)
                    .then(function (results) {
                        vm.results = results.data;
                    })
                    .catch(function (err) {
                        console.error(err);
                    });
            };

            vm.previous = function () {
                vm.currentPage = vm.currentPage - 1;
                vm.pageIndex = vm.pageIndex - recordPerPage;
                dbService.defaultAppPaging(vm.pageIndex, recordPerPage)
                    .then(function (results) {
                        vm.results = results.data;
                    })
                    .catch(function (err) {
                        console.error(err);
                    });
            };
        }
    }

    DefaultAppCtrl.$inject  = ["$q", "dbService", "AuthService"];

    function CustomAppCtrl($q, dbService, AuthService){
        var vm = this;
        vm.results = [];
        vm.pageIndex = 0;
        const recordPerPage = 3;
        vm.totalRecords = 0;
        vm.totalPages = 0;
        vm.currentPage = 1;
        vm.isUserLogon = AuthService.isLoggedIn();

        var defer = $q.defer();
        vm.err = null;

        if(vm.isUserLogon) {
            dbService.getTotalCustomApps();


            dbService.customAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });

            dbService.getTotalCustomApps()
                .then(function (results) {
                    vm.totalRecords = results.data;
                    vm.totalPages = Math.ceil(vm.totalRecords / recordPerPage);
                    console.info(vm.totalPages);
                })
                .catch(function (err) {
                    console.error(err);
                });
        }

        vm.next = function () {
            console.info("Why bug?");
            vm.currentPage = vm.currentPage + 1;
            vm.pageIndex = vm.pageIndex + recordPerPage;
            dbService.customAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });
        };

        vm.previous = function () {
            vm.currentPage = vm.currentPage - 1;
            vm.pageIndex = vm.pageIndex - recordPerPage;
            dbService.customAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });
        };

    }

    CustomAppCtrl.$inject  = ["$q", "dbService", "AuthService"];

})();