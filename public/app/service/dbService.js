(function () {
    angular
        .module("app")
        .service("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService($http, $q) {
        var service = this;

        service.getLocalProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/profile/")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        service.getAllSocialLoginsProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/social/profiles")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };
    }
})();
