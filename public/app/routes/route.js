(function () {

    angular
        .module("app")
        .config(UserConfig);

    function UserConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state("home", {
              //  url: "/?join&signup&referal",
                url: "/",
                views: {
                    "nav":{
                        templateUrl: "/app/views/navbar.html"
                    },
                    "content":{
                        templateUrl: "/app/views/home.html"
                    }
                },
                controller: function($location, $stateParams) {
                    if($stateParams.join || $stateParams.signup) {
                        angular.element("#loginSignup").click()
                    }
                }
            })
            .state("filter", {
                url: "/filter",
                views: {
                    "nav":{
                        templateUrl: "/app/views/navbar.html"
                    },
                    "content":{
                        templateUrl: "/app/views/filter.html",
                    }     
                },
                controller: "FilterCtrl as ctrl"
            })
            .state("detail", {
                url: "/detail",
                views: {
                    "nav":{
                        templateUrl: "/app/views/navbar.html"
                    },
                    "content":{
                        templateUrl: "/app/views/detail.html",
                    }
                },
                controller: "DetailCtrl as ctrl"
            })
        ;
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise("/");
    }

    UserConfig.$inject = ["$stateProvider", "$urlRouterProvider", "$locationProvider"];

})();


