var LocalStrategy = require("passport-local").Strategy;
var FacebookStrategy = require("passport-facebook").Strategy
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var models = require('./models/index');
var bcrypt   = require('bcrypt-nodejs');

var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/config.json')[env];

//Setup local strategy
module.exports = function (app, passport) {
    function authenticate(username, password, done) {

        models.users.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            if(!result){
                return done(null, false);
            }else{
                if(bcrypt.compareSync(password , result.encrypted_password)){
                    return done(null, result);
                }else{
                    return done(null, false);
                }
            }
        }).catch(function(err){
            return done(err, false);
        });


    }

    function verifyCallback(accessToken, refreshToken, profile, done) {
        if(profile.provider === 'google' || profile.provider === 'facebook'|| profile.provider === 'linkedin'){
            id = profile.id;
            email = profile.emails[0].value;
            displayName = profile.displayName;
            provider_type = profile.provider;
            models.users.findOrCreate({where: {email: email}, defaults: {email: email, name: displayName}})
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    models.authentication_provider.findOrCreate({where: {userid: user.id, providerType: provider_type},
                        defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
                        .spread(function(provider, created) {
                            console.log(provider.get({
                                plain: true
                            }));
                            console.log(created);
                        });
                    done(null, user);
                });
        }else{
            done(null, false);
        }
    }

    passport.use(new FacebookStrategy({
        clientID: config.Facebook_key,
        clientSecret: config.Facebook_secret,
        callbackURL: config.Facebook_callback_url,
        profileFields: ['id', 'first_name', 'last_name', 'picture', 'birthday', 'email']
    }, verifyCallback))

    passport.use(new LinkedInStrategy({
        clientID: config.linkedin_key,
        clientSecret: config.Linkedin_secret,
        callbackURL: config.Linkedin_callback_url,
        scope: ['r_emailaddress', 'r_basicprofile'],
    }, verifyCallback));

    passport.use(new GoogleStrategy({
        clientID: config.GooglePlus_key,
        clientSecret: config.GooglePlus_secret,
        callbackURL: config.GooglePlus_callback_url
    }, verifyCallback))
    
    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        models.users.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, user);
            }
        }).catch(function(err){
            done(err, user);
        });
    });

};


