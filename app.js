var path = require("path");
var express = require("express");
var app = express();

var passport = require("passport");
var bodyParser = require("body-parser");
var session = require("express-session");
var flash    = require('connect-flash');
var cookieParser = require('cookie-parser');

const PORT = process.argv[2] || process.env.APP_PORT || 3000;

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "nus-iss-stackup",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

// connect flash
app.use(flash());

require('./auth.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./routes.js')(app); // load our routes and pass in our app and fully configured passport

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.listen(PORT, function(){
    console.log("Server is running now", PORT)
});

